namespace SyntaxMethodApplication

open System
open Avalonia
open Avalonia
open MathNet.Spatial.Euclidean
open MathNet.Spatial.Euclidean
open SyntaxMethodApplication.Grammar
open SyntaxMethodApplication.Grammar.Tokens

module Verificator =
    let verifyTokens (tokens: List<Token>, grammarItem: GrammarItem): bool =
        List.exists (fun (offset: int) ->
            let shiftedTokens = (List.skip offset tokens) @ (List.take offset tokens)
            grammarItem.Match(tokens)(Grammar.TailMatcher)) [0..tokens.Length] 
        
    let verify (points: List<Point>, grammarItem: GrammarItem): bool =
        let allPoints: List<Point2D> = List.map (fun (point: Point) -> Point2D(point.X, point.Y)) points
        let mutable tokens: List<Token> = []
        let mutable length = allPoints.Length 
        for index in 0..(length - 1) do
            let previousPoint = allPoints.[(index - 1 + length) % length] 
            let currentPoint = allPoints.[index]
            let nextPoint = allPoints.[(index + 1) % length]
            let vector1 = previousPoint.VectorTo(currentPoint)
            let vector2 = currentPoint.VectorTo(nextPoint)
            let angle = vector1.AngleTo(vector2).Radians
            tokens <- tokens @ [ AngleToken(angle); LengthToken(vector2.Length) ]

        verifyTokens(tokens, grammarItem)
            

 