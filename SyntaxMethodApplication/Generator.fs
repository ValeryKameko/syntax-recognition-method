namespace SyntaxMethodApplication

open System
open Avalonia
open MathNet.Spatial.Euclidean
open MathNet.Spatial.Units
open SyntaxMethodApplication.Grammar
open SyntaxMethodApplication.Grammar.Tokens

module Generator =
    let generate(grammar: GrammarItem, minPoint: Point2D, maxPoint: Point2D): List<Point2D> =
        let random = Random()
        let tokens =  grammar.Generate random
        let mutable point = Point2D(
                                random.NextDouble() * (maxPoint.X - minPoint.X) + minPoint.X,
                                random.NextDouble() * (maxPoint.Y - minPoint.Y) + minPoint.Y)
        let mutable vector = Vector2D(1.0, 0.0)
        let mutable points = []
        for token in tokens do
            match token with
            | :? LengthToken ->
                let lengthToken = token :?> LengthToken
                point <- point + vector.ScaleBy(lengthToken.Length)
            | :? AngleToken ->
                let angleToken = token :?> AngleToken
                vector <- angleToken.Angle |> Angle.FromRadians |> vector.Rotate
                point <- point + vector
                points <- points @ [point]
            | _ -> raise <| ArgumentException("Not valid generated token")
        Console.WriteLine()
        points