namespace SyntaxMethodApplication.Grammar

open System
open SyntaxMethodApplication.Grammar.Tokens

type TokenGrammarItem(token: Token) =
    inherit GrammarItem()
    
    member this.Token = token
    
    override this.Match(tokens)(MatcherFunc tailMatcherFunc) =
        if not <| List.isEmpty tokens then
            let firstToken = List.head tokens
            let tailTokens = List.skip 1 tokens
            this.Token.Match(firstToken) && (tailMatcherFunc(tailTokens)(Grammar.TailMatcher))
        else
            false

    override this.Generate(random) =
        [this.Token.Generate(random)]