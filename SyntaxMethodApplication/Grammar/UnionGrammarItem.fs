namespace SyntaxMethodApplication.Grammar

type UnionGrammarItem(grammarItems: List<GrammarItem>) =
    inherit GrammarItem()
    
    member this.GrammarItems = grammarItems
    
    override this.Match(tokens)(tailMatcherFunc) =
        List.exists (fun (grammarItem: GrammarItem) ->
            grammarItem.Match(tokens)(tailMatcherFunc))
            this.GrammarItems

    override this.Generate(random) =
        let index = random.Next(0, this.GrammarItems.Length - 1)
        this.GrammarItems.[index].Generate(random)