namespace SyntaxMethodApplication.Grammar

open System
open SyntaxMethodApplication.Grammar.Tokens

[<AbstractClass>]
type GrammarItem() =
    abstract member Match: List<Token> -> MatcherFunc -> bool
    abstract member Generate: Random -> List<Token>