namespace SyntaxMethodApplication.Grammar

open System
open SyntaxMethodApplication.Grammar.Tokens

type MatcherFunc = MatcherFunc of (List<Token> -> MatcherFunc -> bool)

module Grammar =
    let TailMatcher: MatcherFunc = MatcherFunc (fun tokens _tailMatcherFunc -> List.isEmpty tokens) 