namespace SyntaxMethodApplication.Grammar

open System

type ConcatGrammarItem(grammarItems: List<GrammarItem>) =
    inherit GrammarItem()
    
    member this.GrammarItems = grammarItems
    
    override this.Match(tokens)(MatcherFunc tailMatcherFunc) =
        let mutable count = 0
        
        let rec matcher = (fun tokens innerTailMatcherFunc ->
            if count = this.GrammarItems.Length then 
                tailMatcherFunc(tokens)(Grammar.TailMatcher)
            else
                count <- count + 1
                this.GrammarItems.[count - 1].Match tokens (MatcherFunc matcher)
            )
        matcher(tokens)(MatcherFunc matcher)

    override this.Generate(random) =
        List.map (fun (item: GrammarItem) -> item.Generate(random)) this.GrammarItems |> List.concat