namespace SyntaxMethodApplication.Grammar

open System

type CountGrammarItem(grammarItem: GrammarItem, minCount: int, maxCount: int) =
    inherit GrammarItem()
    
    new(grammarItem: GrammarItem, count: int) =
        CountGrammarItem(grammarItem, count, count)
    
    new(grammarItem: GrammarItem) =
        CountGrammarItem(grammarItem, 0, 1)
    
    member this.GrammarItem = grammarItem
    member this.MinCount = minCount
    member this.MaxCount = maxCount
    
    override this.Match(tokens)(tailMatcherFunc) =
        let mutable count = 0
        
        let rec matcher = (fun tokens (MatcherFunc tailMatcherFunc) ->
            if this.MinCount <= count && count <= this.MaxCount then// && tailMatcherFunc(tokens)(Grammar.TailMatcher) then
                true
            elif count > this.MaxCount then
                false
            else
                count <- count + 1
                this.GrammarItem.Match(tokens)(MatcherFunc matcher)
            )
        matcher(tokens)(tailMatcherFunc)

    override this.Generate(random) =
        let count = random.Next(this.MinCount, this.MaxCount)
        Seq.init count (fun _ -> this.GrammarItem.Generate(random)) |> List.concat
             