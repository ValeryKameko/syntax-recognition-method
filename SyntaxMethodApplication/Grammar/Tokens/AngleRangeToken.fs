namespace SyntaxMethodApplication.Grammar.Tokens

type AngleRangeToken(minAngle: double, maxAngle: double) =
    inherit Token()
    
    member this.MinAngle = minAngle
    member this.MaxAngle = maxAngle
    
    override this.Match(token) =
        match token with
        | :? AngleToken ->
            let angleToken = token :?> AngleToken
            this.MinAngle <= angleToken.Angle && angleToken.Angle <= this.MaxAngle
        | _ -> false
        
    override this.Generate(random) =
        let angle = random.NextDouble() * (this.MaxAngle - this.MinAngle) + this.MinAngle
        upcast AngleToken(angle)