namespace SyntaxMethodApplication.Grammar.Tokens

open System

[<AbstractClass>]
type Token() =
    abstract member Match: Token -> bool
    abstract member Generate: Random -> Token
