namespace SyntaxMethodApplication.Grammar.Tokens

open System

type LengthRangeToken(minLength: double, maxLength: double) =
    inherit Token()
    
    let ACCURACY = 0.1 
    
    member this.MinLength = minLength
    member this.MaxLength = maxLength
    
    override this.Match(token) =
        match token with
        | :? LengthToken ->
            let lengthToken = token :?> LengthToken
            this.MinLength - ACCURACY * lengthToken.Length <= lengthToken.Length && lengthToken.Length <= this.MaxLength + ACCURACY * lengthToken.Length
        | _ -> false
        
    override this.Generate(random) =
        let length = random.NextDouble() * (this.MaxLength - this.MinLength) + this.MinLength
        upcast LengthToken(length)