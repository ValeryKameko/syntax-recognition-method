namespace SyntaxMethodApplication.Grammar.Tokens

open System

type LengthToken(length: double) =
    inherit Token()
    
    let ACCURACY = 0.1
    
    member this.Length = length
    
    override this.Match(token) =
        match token with
        | :? LengthToken ->
            let lengthToken = token :?> LengthToken
            let diffLength = this.Length - lengthToken.Length
            abs(diffLength) < (this.Length * ACCURACY)
        | _ -> false
        
    override this.Generate(random) =
        let length = this.Length * ((random.NextDouble() * 2.0 - 1.0) * ACCURACY + 1.0)
        LengthToken(length) :> _