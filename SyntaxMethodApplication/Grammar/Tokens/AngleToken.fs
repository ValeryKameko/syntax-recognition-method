namespace SyntaxMethodApplication.Grammar.Tokens

open System

type AngleToken(angle: double) =
    inherit Token()
    
    let ACCURACY = Math.PI / 10.0
    
    member this.Angle =
        angle % (2.0 * Math.PI)
    
    override this.Match(token) =
        match token with
        | :? AngleToken ->
            let angleToken = token :?> AngleToken
            let diffAngle = this.Angle - angleToken.Angle
            abs(diffAngle) < 2.0 * ACCURACY
        | _ -> false
        
    override this.Generate(random) =
        let mutable angle = random.NextDouble() * 2.0 * ACCURACY + this.Angle - ACCURACY
        if angle < 0.0 then angle <- angle + 2.0 * Math.PI
        upcast AngleToken(angle)