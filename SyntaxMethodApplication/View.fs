﻿namespace SyntaxMethodApplication

open Avalonia
open Avalonia
open Avalonia.Controls.Shapes
open Avalonia.FuncUI.Components
open Avalonia.Media
open Avalonia.VisualTree
open MathNet.Spatial.Euclidean
open MathNet.Spatial.Euclidean
open MessageBox.Avalonia
open SyntaxMethodApplication.Grammar
open SyntaxMethodApplication.Grammars

module Counter =
    open Avalonia.Controls
    open Avalonia.FuncUI.DSL
    open Avalonia.Layout
    
    type State = {
        count : int
        points : List<Point>
        lastPoint : Point
        isMoving : bool
        isComplete : bool
        currentGrammar : GrammarView
        window: Window
    }
    let init window = {
        points = []
        lastPoint = Point(0.0, 0.0)
        count = 0
        isMoving = false
        isComplete = true
        currentGrammar = Grammars.grammarViews.Head
        window = window
    }

    type Msg =
        | ChangeGrammar of GrammarView
        | PointFlush
        | PointClear
        | PointMove of Point
        | PointAdd of Point
        | PointComplete
        | Generate
        | Verify

    let update (msg: Msg) (state: State) : State =
        match msg with
        | PointClear ->
            if state.isMoving
                then { state with points = state.points @ [state.lastPoint] }
                else state
        | PointFlush -> 
            { state with isMoving = false }
        | PointAdd point ->
            let newState = { state with isMoving = true; isComplete = false }
            if state.isComplete
                then { newState with points = [] }
                else newState
        | PointComplete ->
            { state with
                isComplete = true
                isMoving = false
            }    
        | PointMove point ->
            { state with lastPoint = point }
        | ChangeGrammar newGrammar ->
            { state with currentGrammar = newGrammar }
        | Generate ->
            let generatedPoints = Generator.generate(state.currentGrammar.grammar, Point2D(100.0, 100.0), Point2D(700.0, 300.0))
            let points = List.map (fun (item: Point2D) -> Point(item.X, item.Y)) generatedPoints
            { state with points = points }
        | Verify ->
            let result = Verificator.verify(state.points, state.currentGrammar.grammar)
            let resultString = if result then "Match" else "Not Match"
            MessageBoxManager.GetMessageBoxStandardWindow("Result", resultString).ShowDialog(state.window)
            state
    
    let view (state: State) (dispatch) =
        Grid.create [
            Grid.columnDefinitions "1*, 200"
            Grid.children [
                Canvas.create [
                    Grid.column 0
                    Canvas.minWidth 800.0
                    Canvas.minHeight 600.0
                    Canvas.background "#ffffff" 
                    Canvas.onPointerLeave (fun _event -> dispatch PointFlush)
                    Canvas.onPointerPressed (fun event ->
                                             let point = event.GetCurrentPoint(event.Source :?> IVisual)
                                             if point.Properties.IsRightButtonPressed then dispatch <| PointComplete
                                             elif point.Properties.IsLeftButtonPressed then dispatch <| PointAdd point.Position)
                    Canvas.onPointerReleased (fun event -> dispatch PointClear)
                    Canvas.onPointerMoved (fun event ->
                                        let point = event.GetCurrentPoint(event.Source :?> IVisual)
                                        dispatch <| PointMove point.Position)
                    Canvas.children [
                        let points = if state.isMoving
                                     then state.points @ [state.lastPoint]
                                     else state.points
                        if not state.isComplete
                        then Polyline.create [
                            Polyline.points points
                            Polyline.stroke "#000000"
                            Polyline.strokeThickness 5.0
                            ]
                        else Polygon.create [
                            Polygon.points points
                            Polygon.stroke "#000000"
                            Polyline.strokeThickness 5.0
                        ]
                    ]
                ]
                StackPanel.create [
                    Grid.column 1
                    StackPanel.orientation Orientation.Vertical
                    StackPanel.children [
                        TextBlock.create [
                            TextBlock.fontSize 17.0
                            TextBlock.textAlignment TextAlignment.Center
                            TextBlock.text "Controls"
                        ]
                        ComboBox.create [
                            ComboBox.dataItems Grammars.grammarViews
                            ComboBox.selectedItem state.currentGrammar
                            ComboBox.selectedItem state.currentGrammar
                            ComboBox.onSelectedItemChanged (fun obj ->
                                match obj with
                                | :? GrammarView -> obj :?> GrammarView |> ChangeGrammar |> dispatch)
                            ComboBox.itemTemplate (
                                DataTemplateView<GrammarView>.create (fun data ->
                                    TextBlock.create [
                                        TextBlock.text data.name 
                                    ]
                                )
                            )
                        ]
                        Button.create [
                            Button.content "Generate"
                            Button.onClick (fun _ -> dispatch Generate)
                        ]
                        Button.create [
                            Button.content "Verify"
                            Button.onClick (fun _ -> dispatch Verify)
                        ]

                    ]
                ]
            ]
        ]