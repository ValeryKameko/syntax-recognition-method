namespace SyntaxMethodApplication

open System
open MathNet.Spatial.Units
open SyntaxMethodApplication.Grammar
open SyntaxMethodApplication.Grammar.Tokens

module Grammars =
    type GrammarView = {
        name : string
        grammar : GrammarItem
    }
    
    let rectangleGrammar = CountGrammarItem(
                                               ConcatGrammarItem [
                                                   TokenGrammarItem <| AngleToken(Math.PI / 2.0)
                                                   TokenGrammarItem <| LengthRangeToken(100.0, 200.0)
                                               ], 4
                                           )
    
    let starGrammar = ConcatGrammarItem [
                                            CountGrammarItem(
                                                ConcatGrammarItem [
                                                    TokenGrammarItem <| AngleToken(Math.PI * 9.0 / 5.0)
                                                    TokenGrammarItem <| LengthRangeToken(30.0, 50.0)
                                                    TokenGrammarItem <| AngleToken(Math.PI * 7.0 / 10.0)
                                                    TokenGrammarItem <| LengthRangeToken(30.0, 50.0)
                                                ], 4
                                            )
                                       ]
    
    
    
    let manGrammar =
        let headGrammar = ConcatGrammarItem [
            TokenGrammarItem <| LengthRangeToken(30.0, 40.0)
            TokenGrammarItem <| AngleToken(Math.PI / 2.0)
            TokenGrammarItem <| LengthRangeToken(30.0, 40.0)
            TokenGrammarItem <| AngleToken(Math.PI / 2.0)
            TokenGrammarItem <| LengthRangeToken(30.0, 40.0)
        ]
        let armTokens: GrammarItem List = [
            TokenGrammarItem <| AngleToken(0.0)
            TokenGrammarItem <| LengthRangeToken(100.0, 120.0)
            TokenGrammarItem <| AngleToken(Math.PI)
            TokenGrammarItem <| LengthRangeToken(50.0, 60.0)
            TokenGrammarItem <| AngleToken(Math.PI / 2.0)
            TokenGrammarItem <| LengthRangeToken(10.0, 15.0)
            TokenGrammarItem <| AngleToken(Math.PI / 2.0)
            TokenGrammarItem <| LengthRangeToken(120.0, 130.0)
            TokenGrammarItem <| AngleToken(Math.PI / 2.0)
            TokenGrammarItem <| LengthRangeToken(30.0, 40.0)
            TokenGrammarItem <| AngleToken(Math.PI * 3.0 / 2.0)
        ]
        let bodyGrammar = ConcatGrammarItem ([
            TokenGrammarItem <| AngleToken(Math.PI * 3.0 / 2.0)
            ] @ armTokens @ [headGrammar] @ List.rev armTokens)
        bodyGrammar
    
    
    let grammarViews: List<GrammarView> = [
                      { name = "Rectangle"; grammar = rectangleGrammar }
                      { name = "Star"; grammar = starGrammar }
                      { name = "Man"; grammar = manGrammar }
                      ]